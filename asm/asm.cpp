﻿#include <stdio.h>

/*
	
			   88                  ,d8               8b,
			   ""                ,8P'                 `Y8,
								,8P                     Y8,
	,adPPYba,  88  8b,dPPYba,   88      8b,     ,d8      88
	I8[    ""  88  88P'   `"8a  88       `Y8, ,8P'       88
	 `"Y8ba,   88  88       88  `8b        )888(        d8'
	aa    ]8I  88  88       88   `8b,    ,d8" "8b,    ,d8'
	`"YbbdP"'  88  88       88     "Y8  8P'     `Y8  8Y"
	

*/

/*
	sin(x) = sum{ (-1)^k * x^(2k+1) / (2k+1)! }, где k = 0...inf,
	пока модуль очередного члена ряда больше _e

	Если _e < 0, то функция вернет 2
*/
double sin(const double _x, const double _e = 1e-5)
{
	/*************************************************************
	 * r7 = epsilon (const)
	 * r6 = сумма
	 * r5 = x^2 (const)
	 * r4 = 1 (const)
	 * r3 = 2 (const)
	 * r2 = член ряда для суммы
	 * r1 = член факториала = k*2
	 * r0 = доп. член факториала (для умножения вместо деления)
	 *************************************************************/
	int _trunc_x;
	__asm {
		finit
		fld		qword ptr _e	// r7 = epsilon = _e
		ftst
		fstsw	ax
		sahf
		jae		_APPROVE_
		fld1
		fadd	st, st
		jmp		_EPILOGUE_

	_APPROVE_:
		fld		qword ptr _x	// r6 = сумма = x

		// x = x - trunc(x / 2pi) * 2pi; <==> x %= 2pi;
		fldpi
		fadd	st, st		// r5 = 2pi
		fld		st(1)		// r4 = x
		fdiv	st, st(1)	// r4 = x / 2pi
		fisttp	dword ptr _trunc_x	// trunc(st), st = r5
		fild	dword ptr _trunc_x	// r4 = trunc(x / 2pi)
		fmulp	st(1), st	// r5 = trunc(x / 2pi) * 2pi
		fsubp	st(1), st	// r6 = x % 2pi

		fld		st
		fmul	st, st			// r5 = x^2
		fld1					// r4 = 1
		fld		st
		fadd	st, st			// r3 = 2
		fld		st(2)
		fmul	st, st(4)		// r2 = x^3
		fld		st(1)			// r1 = 2

	_CYCLE_:
		fld		st
		fadd	st, st(4)		// r0 = 2k+1
		fmul	st, st(1)		// r0 *= r1 <==> 2k * (2k+1)
		fdivp	st(2), st		// r2 /= r0

		fadd	st, st(2)		// r1 += 2 <==> 2k+2 - для след. итерации
		fxch	st(1)
			
		fchs				// меняет знак у r1, т.е. у члена ряда
		fadd	st(5), st	// сумма := r6 += r1, т.е. прибавляем член ряда

		fld		st
		fabs				// r0 = |r1| = |член ряда|
		fcomip	st, st(7)
		jbe 	_END_
			
		fmul	st, st(4) // r1 *= x^2
		fxch	st(1)
		jmp		_CYCLE_

	_END_:
		fld		st(5) // r0 = r6 = сумма
	_EPILOGUE_:
	}
}

int main()
{
	const char _msg1[] = "input <x> <epsilon>:";
	const char _msg2[] = "absolute error:";
	const char _scanf_params[] = "%lf %lf";
	const char _printf_params[] = "%.16lf\n";
	double _x, _e;

	__asm {
		// puts(_msg1);
		lea		eax, _msg1
		push	eax
		call	puts
		add		esp, 4

		// scanf_s("%lf %lf", &_x, &_e);
		lea		eax, _e
		push	eax
		lea		eax, _x
		push	eax
		lea		eax, _scanf_params
		push	eax
		call	scanf_s
		add		esp, 12

		// sin(_x, _e);
		push	dword ptr [_e + 4]
		push	dword ptr _e
		push	dword ptr [_x + 4]
		push	dword ptr _x
		call	sin
		add		esp, 8 // оставляем 8 для вывода результата

		// printf_s("%lf\n", результат sin);
		fstp	qword ptr [esp] // результат sin
		lea		eax, _printf_params
		push	eax
		call	printf_s
		add		esp, 4 // оставляем 8 для сравнения результата
		
		// puts(_msg2);
		lea		eax, _msg2
		push	eax
		call	puts
		add		esp, 4

		// вычисление погрешности
		finit
		fld		qword ptr [esp]
		fld		qword ptr _x
		fsin
		fsub	st, st(1)
		fabs

		// printf_s("%lf\n", результат сравнения);
		fstp	qword ptr [esp] // результат сравнения
		lea		eax, _printf_params
		push	eax
		call	printf_s
		add		esp, 12

		xor		eax, eax
	}
}
